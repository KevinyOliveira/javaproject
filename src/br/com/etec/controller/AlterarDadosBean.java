package br.com.etec.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.ToggleEvent;
import org.primefaces.model.UploadedFile;

import br.com.etec.business.AlunoRN;
import br.com.etec.model.Aluno;


@ManagedBean  
public class AlterarDadosBean {
	
	private Aluno aluno = new Aluno();
	List<Aluno> alunos = new ArrayList<Aluno>();
	List<Aluno> alunosSelecionados = new ArrayList<Aluno>();
	
	AlunoRN service = new AlunoRN();
	
	@PostConstruct
	public void init() {
		alunos = service.listar();
		aluno = new Aluno();

	}
	private UploadedFile abrirpasta;
	private UploadedFile escolherarquivo;
	
	
		public UploadedFile getEscolherarquivo() {
		return escolherarquivo;
		}

		public void setEscolherarquivo(UploadedFile escolherarquivo) {
		this.escolherarquivo = escolherarquivo;
		}

		public UploadedFile getAbrirpasta() {
		return abrirpasta;
		}

		public void setAbrirpasta(UploadedFile abrirpasta) {
		this.abrirpasta = abrirpasta;
		}

			
    public void upload1(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	
    public void upload2(ToggleEvent event) {
    	FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
    FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    
    public void buttonAction(ActionEvent actionEvent) {
		if(aluno.getId() == null){
			service.gravar(aluno);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Dados adicionados com sucesso"));
		    }
		
        else{
        	service.atualizar(aluno);

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Dados Atualizados com sucesso"));
        }
		aluno = new  Aluno();
		alunos = service.listar();
	}
    
    public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public List<Aluno> getAlunosSelecionados() {
		return alunosSelecionados;
	}

	public void setAlunosSelecionados(List<Aluno> alunosSelecionados) {
		this.alunosSelecionados = alunosSelecionados;
	}
}